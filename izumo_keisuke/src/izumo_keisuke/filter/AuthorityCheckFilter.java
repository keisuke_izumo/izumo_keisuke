package izumo_keisuke.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import izumo_keisuke.beans.User;

@WebFilter(urlPatterns= { "/management", "/edit", "/signup" })
public class AuthorityCheckFilter implements  Filter {
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,ServletException{

		List<String> messages = new ArrayList<String>();
		HttpSession session = ((HttpServletRequest)request).getSession();

		String cssPath = ((HttpServletRequest)request).getRequestURI();

		if(!cssPath.contains("/izumo_keisuke/css")) {
			if(session.getAttribute("loginUser") != null) {
				User user = (User)session.getAttribute("loginUser");

				//総務部のとき
				if(user.getBranchId() == 1 && user.getDepartmentId() == 1) {
					chain.doFilter(request,response);
				//総務部じゃないとき
				} else {
					messages.add("権限がありません");
					session.setAttribute("errorMessages", messages);
					((HttpServletResponse)response).sendRedirect("./");
				}
			} else {
				messages.add("ログインしてください");
				session.setAttribute("errorMessages", messages);
				((HttpServletResponse)response).sendRedirect("login");
			}
		} else {
			chain.doFilter(request,response);
		}
	}
	@Override
	public void init(FilterConfig config) throws ServletException{

	}
	@Override
	public void destroy(){

	}

}
