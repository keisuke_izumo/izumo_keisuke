package izumo_keisuke.service;

import static izumo_keisuke.utils.CloseableUtil.*;
import static izumo_keisuke.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import izumo_keisuke.beans.UserComment;
import izumo_keisuke.dao.CommentDao;

public class CommentService {

	public List<UserComment> getComment() {
		Connection connection = null;
		try {
			connection = getConnection();
			CommentDao commentDao = new CommentDao();
			List<UserComment> ret = commentDao.select(connection);

			commit(connection);
			return ret;

		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
