package izumo_keisuke.service;

import static izumo_keisuke.utils.CloseableUtil.*;
import static izumo_keisuke.utils.DBUtil.*;

import java.sql.Connection;

import izumo_keisuke.beans.Message;
import izumo_keisuke.dao.NewMessageDao;

public class NewMessageService {
	public void register(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			NewMessageDao newMessageDao = new NewMessageDao();
			newMessageDao.insert(connection, message);

			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}



}
