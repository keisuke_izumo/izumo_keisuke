package izumo_keisuke.service;

import static izumo_keisuke.utils.CloseableUtil.*;
import static izumo_keisuke.utils.DBUtil.*;

import java.sql.Connection;

import izumo_keisuke.dao.MessageDeleteDao;

public class MessageDeleteService {
	public void delete(int messageId) {
		Connection connection = null;
		try {
			connection = getConnection();
			MessageDeleteDao messageDeleteDao = new MessageDeleteDao();
			messageDeleteDao.delete(connection, messageId);
			commit(connection);
		} catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
}
