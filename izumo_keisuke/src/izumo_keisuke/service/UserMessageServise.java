package izumo_keisuke.service;

import static izumo_keisuke.utils.CloseableUtil.*;
import static izumo_keisuke.utils.DBUtil.*;

import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import izumo_keisuke.beans.UserMessage;
import izumo_keisuke.dao.UserMessageDao;

public class UserMessageServise {

	public List<UserMessage> getMessage(String searchCategory, String startDate, String endDate) {

		Date date = new Date();
		DateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		String formattedDate=dateFormat.format(date);

		//開始日、終了日の設定
		if(StringUtils.isEmpty(startDate) == true) {
			startDate = "2019-01-01";
		}
		if(StringUtils.isEmpty(endDate) == true) {
			endDate = formattedDate;
		}
		//時刻を加える
		startDate += " 00:00:00";
		endDate += " 23:59:59";

		Connection connection = null;
		try {
	        connection = getConnection();

	        UserMessageDao userMessageDao = new UserMessageDao();
	        List<UserMessage> ret = userMessageDao.getMessages(connection, searchCategory, startDate, endDate);

	        commit(connection);

	        return ret;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
}
