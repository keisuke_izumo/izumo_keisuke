package izumo_keisuke.service;

import static izumo_keisuke.utils.CloseableUtil.*;
import static izumo_keisuke.utils.DBUtil.*;

import java.sql.Connection;

import izumo_keisuke.beans.Comment;
import izumo_keisuke.dao.NewCommentDao;

public class NewCommentService {

	public void register(Comment comment) {
		Connection connection = null;
		try {
			connection = getConnection();
			NewCommentDao newCommentDao = new NewCommentDao();
			newCommentDao.insert(connection, comment);

			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}
}
