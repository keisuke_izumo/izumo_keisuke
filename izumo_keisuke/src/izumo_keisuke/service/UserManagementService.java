package izumo_keisuke.service;

import static izumo_keisuke.utils.CloseableUtil.*;
import static izumo_keisuke.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import izumo_keisuke.beans.UserInfo;
import izumo_keisuke.dao.UserManagementDao;

public class UserManagementService {
	public List<UserInfo> select() {
		Connection connection = null;
		try {
			connection = getConnection();

			UserManagementDao userManagementDao = new UserManagementDao();
			List<UserInfo> ret = userManagementDao.selectUser(connection);

			commit(connection);
			return ret;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
