package izumo_keisuke.service;

import static izumo_keisuke.utils.CloseableUtil.*;
import static izumo_keisuke.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import izumo_keisuke.beans.User;
import izumo_keisuke.beans.UserInfo;
import izumo_keisuke.dao.UserDao;
import izumo_keisuke.utils.CipherUtil;

public class UserService {
	public void register(User user) {
		Connection connection = null;
		try {
			connection = getConnection();
			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);
			UserDao userDao = new UserDao();
			userDao.insert(connection, user);

			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}

	public User select(String loginId) {
		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			User userDuplicate = userDao.select(connection, loginId);

			commit(connection);
			return userDuplicate;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public List<UserInfo> branchSelect() {
		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			List<UserInfo> code = userDao.branchSelect(connection);

			commit(connection);
			return code;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public List<UserInfo> departmentSelect() {
		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			List<UserInfo> code = userDao.departmentSelect(connection);

			commit(connection);
			return code;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
