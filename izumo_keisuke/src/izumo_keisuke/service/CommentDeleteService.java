package izumo_keisuke.service;

import static izumo_keisuke.utils.CloseableUtil.*;
import static izumo_keisuke.utils.DBUtil.*;

import java.sql.Connection;

import izumo_keisuke.dao.CommentDeleteDao;

public class CommentDeleteService {

	public void delete(int commentId) {
		Connection connection = null;
		try {
			connection = getConnection();
			CommentDeleteDao commentDeleteDao = new CommentDeleteDao();
			commentDeleteDao.delete(connection, commentId);

			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
