package izumo_keisuke.service;

import static izumo_keisuke.utils.CloseableUtil.*;
import static izumo_keisuke.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import izumo_keisuke.beans.UserInfo;
import izumo_keisuke.dao.UserEditDao;
import izumo_keisuke.utils.CipherUtil;

public class UserEditService {
	public UserInfo select(int id) {
		Connection connection = null;
		try {
			connection = getConnection();

			UserEditDao userEditDao = new UserEditDao();
			UserInfo user = userEditDao.getUser(connection, id);

			commit(connection);
			return user;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public void update(UserInfo user) {
		Connection connection = null;
		try {
			connection = getConnection();
			if(StringUtils.isEmpty(user.getPassword()) == false) {
				String encPassword = CipherUtil.encrypt(user.getPassword());
				user.setPassword(encPassword);
			}
			UserEditDao userEditDao = new UserEditDao();
			userEditDao.update(connection, user);

			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}
	public List<Integer> paraCheck() {
		Connection connection = null;
		try {
			connection = getConnection();
			UserEditDao userEditDao = new UserEditDao();
			List<Integer> user = userEditDao.paraCheck(connection);

			commit(connection);
			return user;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}
}
