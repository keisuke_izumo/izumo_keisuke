package izumo_keisuke.service;

import static izumo_keisuke.utils.CloseableUtil.*;
import static izumo_keisuke.utils.DBUtil.*;

import java.sql.Connection;

import izumo_keisuke.dao.UserStopedDao;

public class UserStopedService {

	public void stoped(int isStoped, int id) {
		Connection connection = null;
		try {
			connection = getConnection();

			UserStopedDao userStopedDao = new UserStopedDao();
			userStopedDao.stoped(connection, isStoped, id);

			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
