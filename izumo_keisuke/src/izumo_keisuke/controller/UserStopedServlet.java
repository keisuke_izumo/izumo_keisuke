package izumo_keisuke.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import izumo_keisuke.service.UserStopedService;


@WebServlet(urlPatterns = { "/stoped" })
public class UserStopedServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	//停止・復活処理
		int isStoped = Integer.parseInt(request.getParameter("hiddenIsStoped"));
		int id = Integer.parseInt(request.getParameter("hiddenId"));
		if(isStoped == 1) {
			isStoped = 0;
		} else {
			isStoped = 1;
		}
		new UserStopedService().stoped(isStoped, id);

		//ユーザ登録情報を一覧表示
//		List<UserInfo> userList = new UserManagementService().select();
//		request.setAttribute("userList", userList);

		response.sendRedirect("management");
	}

}
