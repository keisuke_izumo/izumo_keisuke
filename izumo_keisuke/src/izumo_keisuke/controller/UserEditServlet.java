package izumo_keisuke.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import izumo_keisuke.beans.User;
import izumo_keisuke.beans.UserInfo;
import izumo_keisuke.service.UserEditService;
import izumo_keisuke.service.UserService;

/**
 * Servlet implementation class editServlet
 */
@WebServlet(urlPatterns = { "/edit" })
public class UserEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    	List<String> errorMessages = new ArrayList<String>();
    	List<Integer> idList = new ArrayList<Integer>();
        HttpSession session = request.getSession();
        User loginUser = (User)session.getAttribute("loginUser");
        //管理画面からの値チェック
    	if(request.getParameter("parameter") == null) {
    		errorMessages.add("パラメータが不正です");
        	request.setAttribute("loginUser", loginUser);
        	session.setAttribute("errorMessages", errorMessages);
    		response.sendRedirect("management");
    	//パラメーターが数字
    	} else if(request.getParameter("parameter").matches("^[0-9]+$")){
    		UserEditService ues = new UserEditService();
    		idList = ues.paraCheck();
    		if(idList.contains(Integer.parseInt(request.getParameter("parameter")))) {
    			//編集ユーザをselect
    			int id = Integer.parseInt(request.getParameter("parameter"));
    	    	UserEditService editService = new UserEditService();
    			UserInfo user = editService.select(id);
    			createList(request);

    			request.setAttribute("loginUser", loginUser);
    			request.setAttribute("user", user);
    			request.getRequestDispatcher("edit.jsp").forward(request, response);
    		} else {
    			errorMessages.add("パラメータが不正です");
            	session.setAttribute("errorMessages", errorMessages);
        		response.sendRedirect("management");
    		}

    	} else {
    		errorMessages.add("パラメータが不正です");
        	session.setAttribute("errorMessages", errorMessages);
    		response.sendRedirect("management");
    	}
	}

    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//更新処理
    	List<String> errorMessages = new ArrayList<String>();
        HttpSession session = request.getSession();
        //支店、役職チェック
        boolean flag = departmentCheck(request);

        if (isValid(request, errorMessages, flag) == true) {
        	String userId = request.getParameter("loginId");
        	User userDuplicate = new UserService().select(userId);
        	UserInfo editUser = getEditUser(request);

        	//ID重複チェック
        	if(userDuplicate == null) {
        		new UserEditService().update(editUser);
	    		response.sendRedirect("management");
        	} else {
		        if(userDuplicate.getId() == Integer.parseInt(request.getParameter("hiddenId"))) {
		    		new UserEditService().update(editUser);
		    		response.sendRedirect("management");
		        } else {
		        	errorMessages.add("そのログインIDはすでに存在しています");
		        	session.setAttribute("errorMessages", errorMessages);
		        	request.setAttribute("parameter", userDuplicate.getId());
		        	request.setAttribute("user", editUser);
		        	createList(request);
		        	request.getRequestDispatcher("edit.jsp").forward(request, response);
		        }
        	}
        } else {
        	session.setAttribute("errorMessages", errorMessages);
        	request.setAttribute("parameter", request.getParameter("hiddenId"));
        	UserInfo editUser = getEditUser(request);
        	createList(request);
        	request.setAttribute("user", editUser);
        	request.getRequestDispatcher("edit.jsp").forward(request, response);
        }
	}

    private UserInfo getEditUser(HttpServletRequest request)
            throws IOException, ServletException {

        UserInfo editUser = new UserInfo();
        editUser.setId(Integer.parseInt(request.getParameter("hiddenId")));
        if(StringUtils.isBlank(request.getParameter("loginId")) == false) {
        	editUser.setLoginId(request.getParameter("loginId"));
        }
        editUser.setName(request.getParameter("name"));
        if(StringUtils.isBlank(request.getParameter("password")) == false) {
        	editUser.setPassword(request.getParameter("password"));
        }
        editUser.setBranchId(Integer.parseInt(request.getParameter("branchList")));
        editUser.setDepartmentId(Integer.parseInt(request.getParameter("departmentList")));
        if(StringUtils.isBlank(request.getParameter("branchName")) == false) {
        	editUser.setBranchName(request.getParameter("branchName"));
        }
        if(StringUtils.isBlank(request.getParameter("departmentName")) == false) {
        	editUser.setDepartmentName(request.getParameter("departmentName"));
        }

        try {
	        SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	        Date createdDate = sdFormat.parse(request.getParameter("hiddenCreatedDate"));
	        editUser.setCreatedDate(createdDate);
	        Date updatedDate = sdFormat.parse(request.getParameter("hiddenUpdatedDate"));
	        editUser.setUpdatedDate(updatedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return editUser;
    }
    private boolean isValid(HttpServletRequest request, List<String> errorMessages, boolean flag) {

    	String loginId = request.getParameter("loginId");
    	String name = request.getParameter("name");
    	String password = request.getParameter("password");
    	String confirmPass = request.getParameter("confirmPass");
    	String branchId = request.getParameter("branchList");
    	String departmentId = request.getParameter("departmentList");

    	if (StringUtils.isBlank(loginId) == true) {
    		errorMessages.add("ログインIDを入力してください");
    	} else {
    		if (!loginId.matches("^[0-9a-zA-Z]+$")) {
    			errorMessages.add("ログインIDは半角英数字で入力してください");
    		}
    		if(loginId.length() <= 5) {
    			errorMessages.add("ログインIDは6文字以上で入力してください");
    		}
    		if(loginId.length() >= 21) {
    			errorMessages.add("ログインIDは20文字以下で入力してください");
    		}
    	}
    	if (StringUtils.isBlank(name) == true) {
    		errorMessages.add("名前を入力してください");
    	} else {
    		if(name.length() >= 11) {
    			errorMessages.add("名前は10文字以下で入力してください");
    		}
    	}
    	if (StringUtils.isBlank(password) == false) {
	    	if (!password.matches("[-_@+*;:#$%&\\w]+")) {
	    		errorMessages.add("パスワードは半角英数字または記号で入力してください");
	    	}
	    	if(password.length() <= 5) {
	    		errorMessages.add("パスワードは6文字以上で入力してください");
	    	}
	    	if(password.length() >= 21) {
	    		errorMessages.add("パスワードは20文字以下で入力してください");
	    	}
	    	if (StringUtils.isBlank(confirmPass) == true) {
	    		errorMessages.add("入力したパスワードと確認用パスワードが一致しません");
	    	} else {
	    		if(!password.equals(confirmPass)) {
	    			errorMessages.add("入力したパスワードと確認用パスワードが一致しません");
	    		}
	    	}
    	}
    	if (StringUtils.isEmpty(branchId) == true) {
    		errorMessages.add("支店を入力してください");
    	}
    	if (StringUtils.isEmpty(departmentId) == true) {
    		errorMessages.add("部署・役職を入力してください");
    	}
    	if (flag == false) {
    		errorMessages.add("支店と部署・役職の組み合わせが不正です");
    	}

    	if (errorMessages.size() == 0) {
    		return true;
    	} else {
    		return false;
    	}
    }
    private void createList(HttpServletRequest request) {
    	//支店セレクトボックス生成用
    	List<UserInfo> branch = new ArrayList<UserInfo>();
    	branch = new UserService().branchSelect();
    	//役職セレクトボックス生成用
    	List<UserInfo> department = new ArrayList<UserInfo>();
    	department = new UserService().departmentSelect();
    	request.setAttribute("branch", branch);
    	request.setAttribute("department", department);
    }
    private boolean departmentCheck(HttpServletRequest request) {
    	String branchId = request.getParameter("branchList");
    	String departmentId = request.getParameter("departmentList");

    	if(branchId.equals("1")) {
    		if((departmentId.equals("1")) || (departmentId.equals("2"))){
    			return true;
    		} else {
    			return false;
    		}
    	} else {
    		if((departmentId.equals("1")) || (departmentId.equals("2"))){
    			return false;
    		} else {
    			return true;
    		}
    	}
    }

}
