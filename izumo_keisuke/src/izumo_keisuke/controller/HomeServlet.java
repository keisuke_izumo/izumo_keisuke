package izumo_keisuke.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import izumo_keisuke.beans.User;
import izumo_keisuke.beans.UserComment;
import izumo_keisuke.beans.UserMessage;
import izumo_keisuke.service.CommentService;
import izumo_keisuke.service.UserMessageServise;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("loginUser");
		request.setAttribute("loginUser", user);
		String searchCategory = request.getParameter("searchCategory");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		request.setAttribute("searchCategory", searchCategory);
		request.setAttribute("startDate", startDate);
		request.setAttribute("endDate", endDate);

		//投稿一覧の取得
		List<UserMessage> messages = new UserMessageServise().getMessage(searchCategory, startDate, endDate);
		request.setAttribute("messageList", messages);
		//コメントの取得
		List<UserComment> commentList = new CommentService().getComment();
		request.setAttribute("commentList", commentList);

		request.getRequestDispatcher("/home.jsp").forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


	}

}
