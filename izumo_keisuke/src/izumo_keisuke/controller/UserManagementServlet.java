package izumo_keisuke.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import izumo_keisuke.beans.UserInfo;
import izumo_keisuke.service.UserManagementService;

@WebServlet(urlPatterns = { "/management" })
public class UserManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ユーザ登録情報を一覧表示
		List<UserInfo> userList = new UserManagementService().select();
		request.setAttribute("userList", userList);

		request.getRequestDispatcher("/management.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
