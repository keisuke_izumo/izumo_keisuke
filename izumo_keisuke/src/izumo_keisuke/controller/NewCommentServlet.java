package izumo_keisuke.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import izumo_keisuke.beans.Comment;
import izumo_keisuke.beans.User;
import izumo_keisuke.service.NewCommentService;

@WebServlet(urlPatterns = { "/newComment" })
public class NewCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<String> errorMessages = new ArrayList<String>();
		HttpSession session = request.getSession();

		if(isValid(request, errorMessages) == true) {
			//コメントのインサート
			User user = (User) session.getAttribute("loginUser");
			Comment comment = new Comment();
			comment.setMessageId(Integer.parseInt(request.getParameter("hiddenMessageId")));
			comment.setText(request.getParameter("newComment"));
			comment.setUserId(user.getId());

			new NewCommentService().register(comment);

			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
		}

	}
	private boolean isValid(HttpServletRequest request, List<String> errorMessages) {
		String newComment = request.getParameter("newComment");

		if(StringUtils.isBlank(newComment)) {
			errorMessages.add("コメントを入力してください");
		} else {
			if(newComment.length() > 500) {
				errorMessages.add("コメントは500文字以下で入力してください");
			}
		}

		if (errorMessages.size() == 0) {
    		return true;
    	} else {
    		return false;
    	}
	}

}
