package izumo_keisuke.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import izumo_keisuke.beans.User;
import izumo_keisuke.beans.UserInfo;
import izumo_keisuke.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    	createList(request);
    	request.getRequestDispatcher("signup.jsp").forward(request, response);
	}
    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	List<String> messages = new ArrayList<String>();
    	HttpSession session = request.getSession();

    	User user = new User();
    	//支店、役職チェック
        boolean flag = departmentCheck(request);
    	if (isValid(request, messages, flag) == true) {

    		//ID重複チェック
    		String userId = request.getParameter("loginId");
	        User userDuplicate = new UserService().select(userId);
	        if(userDuplicate == null) {
	        	user = getUser(request);
	        	new UserService().register(user);
	        	response.sendRedirect("management");
	        } else {
        		messages.add("そのIDはすでに存在しています");
        		session.setAttribute("errorMessages", messages);
        		user = getUser(request);
        		createList(request);
        		request.setAttribute("user", user );
        		request.getRequestDispatcher("signup.jsp").forward(request, response);
	        }
    	} else {
    		session.setAttribute("errorMessages", messages);
    		user = getUser(request);
    		createList(request);
    		request.setAttribute("user", user );
    		request.getRequestDispatcher("signup.jsp").forward(request, response);
    	}

	}
    private User getUser(HttpServletRequest request)
            throws IOException, ServletException {

        User user = new User();
        if(StringUtils.isBlank(request.getParameter("loginId")) == false) {
        	user.setLoginId(request.getParameter("loginId"));
        }
        user.setName(request.getParameter("name"));
    	user.setPassword(request.getParameter("password"));
    	if(StringUtils.isBlank(request.getParameter("branchList")) == false) {
    		user.setBranchId(Integer.parseInt(request.getParameter("branchList")));
    	}
    	if(StringUtils.isBlank(request.getParameter("departmentList")) == false) {
    		user.setDepartmentId(Integer.parseInt(request.getParameter("departmentList")));
    	}
        return user;
    }

    private boolean isValid(HttpServletRequest request, List<String> messages, boolean flag) {
    	String loginId = request.getParameter("loginId");
    	String name = request.getParameter("name");
    	String password = request.getParameter("password");
    	String confirmPass = request.getParameter("confirmPass");
    	String branchId = request.getParameter("branchList");
    	String depatmentId = request.getParameter("departmentList");

    	if (StringUtils.isBlank(loginId) == true) {
    		messages.add("ログインIDを入力してください");
    	} else {
    		if (!loginId.matches("^[0-9a-zA-Z]+$")) {
    			messages.add("ログインIDは半角英数字で入力してください");
    		}
    		if(loginId.length() <= 5) {
    			messages.add("ログインIDは6文字以上で入力してください");
    		}
    		if(loginId.length() >= 21) {
    			messages.add("ログインIDは20文字以下で入力してください");
    		}
    	}
    	if (StringUtils.isBlank(name) == true) {
    		messages.add("名前を入力してください");
    	} else {
    		if(name.length() >= 11) {
    			messages.add("名前は10文字以下で入力してください");
    		}
    	}
    	if (StringUtils.isBlank(password) == true) {
    		messages.add("パスワードを入力してください");
    	} else {
	    	if (!password.matches("[-_@+*;:#$%&\\w]+")) {
	    		messages.add("パスワードは半角英数字または記号で入力してください");
	    	}
	    	if(password.length() <= 5) {
	    		messages.add("パスワードは6文字以上で入力してください");
	    	}
	    	if(password.length() >= 21) {
	    		messages.add("パスワードは20文字以下で入力してください");
	    	}
	    	if (StringUtils.isBlank(confirmPass) == true) {
	    		messages.add("確認用パスワードを入力してください");
	    	} else {
	    		if(!password.equals(confirmPass)) {
	    			messages.add("入力したパスワードと確認用パスワードが一致しません");
	    		}
	    	}
    	}
    	if (StringUtils.isBlank(branchId) == true) {
    		messages.add("支店コードを入力してください");
    	}
    	if (StringUtils.isBlank(depatmentId) == true) {
    		messages.add("部署・役職コードを入力してください");
    	}
    	if (!flag) {
    		messages.add("支店と部署・役職の組み合わせが不正です");
    	}
    	if (messages.size() == 0) {
    		return true;
    	} else {
    		return false;
    	}

    }
    private void createList(HttpServletRequest request) {
    	//支店セレクトボックス生成用
    	List<UserInfo> branch = new ArrayList<UserInfo>();
    	branch = new UserService().branchSelect();
    	//役職セレクトボックス生成用
    	List<UserInfo> department = new ArrayList<UserInfo>();
    	department = new UserService().departmentSelect();
    	request.setAttribute("branch", branch);
    	request.setAttribute("department", department);
    }
    private boolean departmentCheck(HttpServletRequest request) {
    	String branchId = request.getParameter("branchList");
    	String departmentId = request.getParameter("departmentList");

    	if(branchId.equals("1")) {
    		if((departmentId.equals("1")) || (departmentId.equals("2"))){
    			return true;
    		} else {
    			return false;
    		}
    	} else {
    		if((departmentId.equals("1")) || (departmentId.equals("2"))){
    			return false;
    		} else {
    			return true;
    		}
    	}
    }

}
