package izumo_keisuke.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import izumo_keisuke.beans.Message;
import izumo_keisuke.beans.User;
import izumo_keisuke.service.NewMessageService;


@WebServlet(urlPatterns = { "/newMessage" })
public class NewMessagesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    	request.getRequestDispatcher("newMessages.jsp").forward(request, response);
	}

    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<String> errorMessages = new ArrayList<String>();
		HttpSession session = request.getSession();

	    if (isValid(request, errorMessages) == true) {
	    	User user = (User) session.getAttribute("loginUser");
	    	Message message = new Message();
	    	message.setTitle(request.getParameter("title"));
	    	message.setCategory(request.getParameter("category"));
	    	message.setMessage(request.getParameter("message"));
	    	message.setUserId(user.getId());

	    	new NewMessageService().register(message);
	    	response.sendRedirect("./");
	    } else {
	    	session.setAttribute("errorMessages", errorMessages);
	    	Message message = new Message();
	    	message.setTitle(request.getParameter("title"));
	    	message.setCategory(request.getParameter("category"));
	    	message.setMessage(request.getParameter("message"));
	    	request.setAttribute("message", message);
	    	request.getRequestDispatcher("newMessages.jsp").forward(request, response);
	    }

	}
    private boolean isValid(HttpServletRequest request, List<String> errorMessages) {

    	String title = request.getParameter("title");
    	String text = request.getParameter("message");
    	String category = request.getParameter("category");

    	if(StringUtils.isBlank(category)) {
    		errorMessages.add("カテゴリーを入力してください");
    	} else {
    		if(category.length() >= 11) {
    			errorMessages.add("カテゴリーは10文字以下で入力してください");
    		}
    	}
    	if(StringUtils.isBlank(title) == true) {
    		errorMessages.add("件名を入力してください");
    	} else {
    		if(title.length() >= 31) {
    			errorMessages.add("件名は30文字以下で入力してください");
    		}
    	}
    	if(StringUtils.isBlank(text) == true) {
    		errorMessages.add("本文を入力してください");
    	} else {
    		if(text.length() >= 1001) {
    			errorMessages.add("本文は1000文字以下で入力してください");
    		}
    	}
    	if (errorMessages.size() == 0) {
    		return true;
    	} else {
    		return false;
    	}
    }

}
