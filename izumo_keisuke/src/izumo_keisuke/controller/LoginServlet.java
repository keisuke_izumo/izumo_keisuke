package izumo_keisuke.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import izumo_keisuke.beans.User;
import izumo_keisuke.service.LoginService;


@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {

		request.getRequestDispatcher("login.jsp").forward(request, response);
    }

	@Override
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {
		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();

		if (isValid(request, messages) == true) {
			String loginId = request.getParameter("loginId");
			String password = request.getParameter("password");

			LoginService loginService = new LoginService();
			User user = loginService.login(loginId, password);

			if (user != null) {
				session.setAttribute("loginUser", user);
				response.sendRedirect("./");
			} else {
				messages.add("ログインIDまたはパスワードが誤っています");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("loginInfo", loginId);
				request.getRequestDispatcher("/login.jsp").forward(request, response);
			}
		} else {
			session.setAttribute("errorMessages", messages);
			String loginInfo = request.getParameter("loginId");
			request.setAttribute("loginInfo", loginInfo);
			request.getRequestDispatcher("/login.jsp").forward(request, response);
		}
    }
	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

		if (StringUtils.isBlank(loginId) == true) {
    		messages.add("ログインIDを入力してください");
    	}
		if (StringUtils.isBlank(password) == true) {
    		messages.add("パスワードを入力してください");
    	}
		if (messages.size() == 0) {
    		return true;
    	} else {
    		return false;
    	}
	}

}