package izumo_keisuke.dao;

import static izumo_keisuke.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import izumo_keisuke.exception.SQLRuntimeException;

public class MessageDeleteDao {
	public void delete(Connection connection, int messageId) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM messages WHERE id = ? ");
			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, messageId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}
