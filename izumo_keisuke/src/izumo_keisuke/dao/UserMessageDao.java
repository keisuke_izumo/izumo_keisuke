package izumo_keisuke.dao;

import static izumo_keisuke.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import izumo_keisuke.beans.UserMessage;
import izumo_keisuke.exception.SQLRuntimeException;

public class UserMessageDao {
	public List<UserMessage> getMessages(Connection connection, String searchCategory, String startDate, String endDate) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("messages.id as id");
			sql.append(", messages.title as title");
			sql.append(", messages.text as text");
			sql.append(", messages.category as category");
			sql.append(", messages.user_id as user_id");
			sql.append(", users.name as user_name");
			sql.append(", users.name as user_name");
			sql.append(", messages.created_date");
			sql.append(" FROM messages");
			sql.append(" INNER JOIN users ON");
			sql.append(" users.id = messages.user_id");
			sql.append(" WHERE");
			sql.append(" messages.created_date >= ?");
			sql.append(" AND");
			sql.append(" messages.created_date <= ?");
			if(StringUtils.isEmpty(searchCategory) == false) {
				sql.append(" AND");
				sql.append(" messages.category LIKE ?");
			}
			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, startDate);
			ps.setString(2, endDate);
			if(StringUtils.isEmpty(searchCategory) == false) {
				ps.setString(3, "%" + searchCategory + "%");
			}
			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toMessageList(rs);
			Collections.reverse(ret);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	//SELECTの結果を格納
	private List<UserMessage> toMessageList(ResultSet rs) throws SQLException {
		List<UserMessage> ret = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String title = rs.getString("title");
				String category = rs.getString("category");
				String text = rs.getString("text");
				int userId = rs.getInt("user_id");
				String userName = rs.getString("user_name");
				Timestamp createdDate = rs.getTimestamp("created_date");
				UserMessage message = new UserMessage();
				message.setId(id);
				message.setTitle(title);
				message.setCategory(category);
				message.setMessage(text);
				message.setUserId(userId);
				message.setUserName(userName);
				message.setCreatedDate(createdDate);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}
