package izumo_keisuke.dao;

import static izumo_keisuke.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import izumo_keisuke.beans.UserInfo;
import izumo_keisuke.exception.NoRowsUpdatedRuntimeException;
import izumo_keisuke.exception.SQLRuntimeException;

public class UserEditDao {
	//ユーザ情報をSELECT
	public UserInfo getUser(Connection connection, int id) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("select " +
					"users.id, " +
					"users.login_id, " +
					"users.name, " +
					"users.password, " +
					"users.branch_id, " +
					"branches.name as branch_name, " +
					"users.department_id, " +
					"departments.name as department_name, " +
					"users.is_stoped, " +
					"users.created_date, " +
					"users.updated_date " +
					"FROM users " +
					"INNER JOIN " +
					"branches " +
					"ON " +
					"users.branch_id = branches.id " +
					"INNER JOIN " +
					"departments " +
					"ON " +
					"users.department_id = departments.id " +
					"WHERE " +
					"users.id = ?" +
					"");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<UserInfo> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if(2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	//SELECTの結果を格納
	private List<UserInfo> toUserList(ResultSet rs) throws SQLException {
		List<UserInfo> ret = new ArrayList<UserInfo>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int branchId = rs.getInt("branch_id");
				String branchName = rs.getString("branch_name");
				int departmentId = rs.getInt("department_id");
				String departmentName = rs.getString("department_name");
				int isStoped = rs.getInt("is_stoped");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");

				UserInfo user = new UserInfo();
				user.setId(id);
				user.setLoginId(loginId);
				user.setPassword(password);
				user.setName(name);
				user.setBranchId(branchId);
				user.setBranchName(branchName);
				user.setDepartmentId(departmentId);
				user.setDepartmentName(departmentName);
				user.setName(name);
				user.setIsStoped(isStoped);
				user.setCreatedDate(createdDate);
				user.setUpdatedDate(updatedDate);
				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
	public void update(Connection connection, UserInfo user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append("  login_id = ?");
            sql.append(", name = ?");
            if(StringUtils.isEmpty(user.getPassword()) == false) {
            	sql.append(", password = ?");
            }
            sql.append(", branch_id = ?");
            sql.append(", department_id = ?");
            sql.append(", updated_date = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            if(StringUtils.isEmpty(user.getPassword()) == false) {
	            ps.setString(1, user.getLoginId());
	            ps.setString(2, user.getName());
            	ps.setString(3, user.getPassword());
	            ps.setInt(4, user.getBranchId());
	            ps.setInt(5, user.getDepartmentId());
	            ps.setInt(6, user.getId());
            } else {
            	ps.setString(1, user.getLoginId());
	            ps.setString(2, user.getName());
	            ps.setInt(3, user.getBranchId());
	            ps.setInt(4, user.getDepartmentId());
	            ps.setInt(5, user.getId());
            }

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }
	public List<Integer> paraCheck(Connection connection) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM users");

			ps = connection.prepareStatement(sql.toString());


			ResultSet rs = ps.executeQuery();
			List<Integer> userList = allUser(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else {
				return userList;
			}
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<Integer> allUser(ResultSet rs) throws SQLException {
		List<Integer> ret = new ArrayList<Integer>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				ret.add(id);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
