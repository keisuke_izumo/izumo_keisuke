package izumo_keisuke.dao;

import static izumo_keisuke.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import izumo_keisuke.beans.UserInfo;
import izumo_keisuke.exception.SQLRuntimeException;

public class UserManagementDao {
	//全ユーザ情報をSELECT
	public List<UserInfo> selectUser(Connection connection) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("select " +
					"users.id, " +
					"users.login_id, " +
					"users.name, " +
					"users.password, " +
					"users.branch_id, " +
					"branches.name as branch_name, " +
					"users.department_id, " +
					"departments.name as department_name, " +
					"users.is_stoped, " +
					"users.created_date, " +
					"users.updated_date " +
					"FROM users " +
					"INNER JOIN " +
					"branches " +
					"ON " +
					"users.branch_id = branches.id " +
					"INNER JOIN " +
					"departments " +
					"ON " +
					"users.department_id = departments.id " +
					"order by users.login_id" +
					"");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserInfo> userList = allUserList(rs);
			return userList;
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	//SELECTの結果を格納
	private List<UserInfo> allUserList(ResultSet rs) throws SQLException {
		List<UserInfo> ret = new ArrayList<UserInfo>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int branchId = rs.getInt("branch_id");
				String branchName = rs.getString("branch_name");
				int departmentId = rs.getInt("department_id");
				String departmentName = rs.getString("department_name");
				int isStoped = rs.getInt("is_stoped");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");

				UserInfo userInfo = new UserInfo();
				userInfo.setId(id);
				userInfo.setLoginId(loginId);
				userInfo.setPassword(password);
				userInfo.setName(name);
				userInfo.setBranchId(branchId);
				userInfo.setBranchName(branchName);
				userInfo.setDepartmentId(departmentId);
				userInfo.setDepartmentName(departmentName);
				userInfo.setName(name);
				userInfo.setIsStoped(isStoped);
				userInfo.setCreatedDate(createdDate);
				userInfo.setUpdatedDate(updatedDate);
				ret.add(userInfo);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
