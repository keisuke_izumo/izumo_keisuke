package izumo_keisuke.dao;

import static izumo_keisuke.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import izumo_keisuke.beans.User;
import izumo_keisuke.beans.UserInfo;
import izumo_keisuke.exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("login_id");
			sql.append(",password");
			sql.append(",name");
			sql.append(",branch_id");
			sql.append(",department_id");
			sql.append(",is_stoped");
			sql.append(",created_date");
			sql.append(",updated_date");
			sql.append(") VALUES (");
			sql.append(" ?");//ログインID
			sql.append(", ?");//パス
			sql.append(", ?");//名前
			sql.append(", ?");//支店コード
			sql.append(", ?");//役職コード
			sql.append(",0");//停止状態
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranchId());
			ps.setInt(5, user.getDepartmentId());
			ps.executeUpdate();
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	//ログインユーザ情報SELECT
	public User getUser(Connection connection, String loginId, String password) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_id = ? AND password = ? AND is_stoped = 0";

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, loginId);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if(2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	//getUserメソッドのSELECTの結果を格納
	private List<User> toUserList(ResultSet rs) throws SQLException {
		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int branchId = rs.getInt("branch_id");
				int departmentId = rs.getInt("department_id");
				int isStoped = rs.getInt("is_stoped");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");
				User user = new User();
				user.setId(id);
				user.setLoginId(loginId);
				user.setPassword(password);
				user.setName(name);
				user.setBranchId(branchId);
				user.setDepartmentId(departmentId);
				user.setIsStoped(isStoped);
				user.setCreatedDate(createdDate);
				user.setUpdatedDate(updatedDate);
				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
	//ユーザ情報SELECT
	public User select(Connection connection, String loginId) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_id = ?";

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, loginId);
			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			System.out.println(userList.isEmpty());
			if(userList.isEmpty() == false) {
				return userList.get(0);
			} else {
				return null;
			}

		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	public List<UserInfo> branchSelect(Connection connection) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM branches";

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<UserInfo> userList = toBranch(rs);
			if(userList.isEmpty() == false) {
				return userList;
			} else {
				return null;
			}

		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<UserInfo> toBranch(ResultSet rs) throws SQLException {
		List<UserInfo> ret = new ArrayList<UserInfo>();
		try {
			while (rs.next()) {
				int branchId = rs.getInt("id");
				String branchName = rs.getString("name");

				UserInfo userInfo = new UserInfo();
				userInfo.setBranchId(branchId);
				userInfo.setBranchName(branchName);
				ret.add(userInfo);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
	public List<UserInfo> departmentSelect(Connection connection) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM departments";

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<UserInfo> userList = toDepartment(rs);
			if(userList.isEmpty() == false) {
				return userList;
			} else {
				return null;
			}

		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<UserInfo> toDepartment(ResultSet rs) throws SQLException {
		List<UserInfo> ret = new ArrayList<UserInfo>();
		try {
			while (rs.next()) {
				int departmentId = rs.getInt("id");
				String departmentName = rs.getString("name");

				UserInfo userInfo = new UserInfo();
				userInfo.setDepartmentId(departmentId);
				userInfo.setDepartmentName(departmentName);
				ret.add(userInfo);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
