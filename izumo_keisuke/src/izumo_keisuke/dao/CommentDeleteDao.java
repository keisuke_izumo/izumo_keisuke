package izumo_keisuke.dao;

import static izumo_keisuke.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import izumo_keisuke.exception.SQLRuntimeException;

public class CommentDeleteDao {
	public void delete(Connection connection, int commentId) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM comments WHERE id = ?");
			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, commentId);
			ps.executeUpdate();

		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}
}
