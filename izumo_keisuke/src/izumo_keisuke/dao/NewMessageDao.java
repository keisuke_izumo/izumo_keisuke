package izumo_keisuke.dao;

import static izumo_keisuke.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import izumo_keisuke.beans.Message;
import izumo_keisuke.exception.SQLRuntimeException;

public class NewMessageDao {
	public void insert(Connection connection, Message message) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
				sql.append("INSERT INTO messages ( ");
				sql.append(" title");
				sql.append(", text");
				sql.append(", category");
				sql.append(", user_id");
				sql.append(", created_date");
				sql.append(") VALUES (");
				sql.append(" ?");//title
				sql.append(", ?");//text
				sql.append(", ?");//category
				sql.append(", ?");//user_id
				sql.append(", CURRENT_TIMESTAMP"); // created_date
				sql.append(")");

				ps = connection.prepareStatement(sql.toString());
				ps.setString(1, message.getTitle());
				ps.setString(2, message.getMessage());
				ps.setString(3, message.getCategory());
				ps.setInt(4, message.getUserId());
				ps.executeUpdate();

		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}
