package izumo_keisuke.dao;

import static izumo_keisuke.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import izumo_keisuke.beans.UserComment;
import izumo_keisuke.exception.SQLRuntimeException;

public class CommentDao {

	public List<UserComment> select(Connection connection) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT  " +
					"comments.id as id " +
					",comments.message_id as message_id " +
					",comments.text as text " +
					",comments.user_id as user_id " +
					",users.name as user_name " +
					",comments.created_date as created_date " +
					"FROM comments " +
					"INNER JOIN  " +
					"users " +
					"ON " +
					"users.id = comments.user_id";

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<UserComment> commentList = toCommentList(rs);
			if (commentList.isEmpty() == true) {
				return null;
			} else {
				return commentList;
			}
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<UserComment> toCommentList(ResultSet rs) throws SQLException {
		List<UserComment> ret = new ArrayList<UserComment>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				int messageId = rs.getInt("message_id");
				String text = rs.getString("text");
				int userId = rs.getInt("user_id");
				String userName = rs.getString("user_name");
				Timestamp createdDate = rs.getTimestamp("created_date");
				UserComment comment = new UserComment();
				comment.setId(id);
				comment.setMessageId(messageId);
				comment.setText(text);
				comment.setUserId(userId);
				comment.setUserName(userName);
				comment.setCreatedDate(createdDate);
				ret.add(comment);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
