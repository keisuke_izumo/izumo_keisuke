<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>ログイン</title>
        <link href="./css/login.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="login">
	        <c:if test="${ not empty errorMessages }">
	            <div class="errorMessages">
	                <ul>
	                    <c:forEach items="${errorMessages}" var="message">
	                        <li><c:out value="${message}" />
	                    </c:forEach>
	                </ul>
	            </div>
	            <c:remove var="errorMessages" scope="session"/>
	        </c:if>
			<h2 class="login-header">ログイン</h2>

            <form action="login" method="post" class="login-container">
                <p><input name="loginId" id="loginId" value="${loginInfo}" placeholder="ログインID"/></p>

                <p><input name="password" type="password" id="password" placeholder="パスワード"/></p>

                <p><input type="submit" value="ログイン" /></p>
            </form>
        </div>
        <div class="copyright"> Copyright(c)Keisuke Izumo</div>
    </body>
</html>