<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:useBean id="date" class="java.util.Date"/>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>ユーザ編集画面</title>
		<link href="./css/edit.css" rel="stylesheet" type="text/css">
		<script>
			function disp(){
				if(window.confirm("この内容で登録しますか？")) {
					return true;
				} else {
					return false;
				}
			}
		</script>
	</head>
	<body>
		<div class="menu">
			<ul>
				<li><a href="./">HOME</a></li>
				<li><a href="management">ユーザ管理</a></li>
				<c:if test="${ not empty loginUser }">
					<li id="logout"><a href="logout">ログアウト</a></li>
				</c:if>
			</ul>
		</div>
		<div class="main-contents">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
			<form action="edit" method="post" onsubmit="return disp();">
				<table class="editTable">
					<tr>
						<th>ログインID</th>
						<td>
							<input type="hidden" name="hiddenId" value="${user.id}" />
							<input type="text" name="loginId" value="${user.loginId}">
						</td>
					</tr>
					<tr>
						<th>名前</th>
						<td>
							<input type="text" name="name" value="${user.name}">
						</td>
					</tr>
					<tr>
						<th>パスワード</th>
						<td>
							<input type="password" name="password">
						</td>
					</tr>
					<tr>
						<th>パスワード確認用</th>
						<td>
							<input type="password" name="confirmPass">
						</td>
					</tr>
					<c:if test="${loginUser.id != user.id }">
						<tr>
							<th>支店名</th>
							<td>
								<select name="branchList">
									<c:forEach items="${branch}" var="branch">
										<c:choose>
											<c:when test="${user.branchId == branch.branchId }">
												<option value="${branch.branchId }" selected><c:out value="${branch.branchName}" /></option>
											</c:when>
											<c:otherwise>
												<option value="${branch.branchId }"><c:out value="${branch.branchName}" /></option>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</select>
							</td>
						</tr>
						<tr>
							<th>部署・役職名</th>
							<td>
								<select name="departmentList">
									<c:forEach items="${department}" var="department">
										<c:choose>
											<c:when test="${user.departmentId == department.departmentId }">
												<option value="${department.departmentId }" selected><c:out value="${department.departmentName}" /></option>
											</c:when>
											<c:otherwise>
												<option value="${department.departmentId }"><c:out value="${department.departmentName}" /></option>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</select>
							</td>
						</tr>
					</c:if>
					<c:if test="${loginUser.id == user.id }">
						<tr>
							<th>支店名</th>
							<td>
								<c:out value="${user.branchName }"></c:out>
								<input type="hidden" name="branchName" value="${user.branchName}" />
								<input type="hidden" name="branchList" value="${user.branchId}" />
							</td>
						</tr>
						<tr>
							<th>部署・役職名</th>
							<td>
								<c:out value="${user.departmentName }"></c:out>
								<input type="hidden" name="departmentName" value="${user.departmentName}" />
								<input type="hidden" name="departmentList" value="${user.departmentId}" />
							</td>
						</tr>
					</c:if>
					<tr>
						<th>停止状態</th>
						<td>
							<c:choose>
		  						<c:when test="${userList.isStoped == '1'}"><c:set var="isStoped">停止</c:set></c:when>
		  						<c:otherwise><c:set var="isStoped">停止してない</c:set></c:otherwise>
							</c:choose>
							<input type="text" value="<c:out value="${isStoped}" />" disabled="disabled" />
						</td>
					</tr>
					<tr>
						<th>登録日</th>
						<td>
							<fmt:formatDate value="${user.createdDate}" pattern="yyyy-MM-dd HH:mm:ss" />
							<input type="hidden" name="hiddenCreatedDate" value="<fmt:formatDate value="${user.createdDate}" pattern="yyyy-MM-dd HH:mm:ss" />" />
						</td>
					</tr>
					<tr>
						<th>更新日</th>
						<td>
							<fmt:formatDate value="${user.updatedDate}" pattern="yyyy-MM-dd HH:mm:ss" />
							<input type="hidden" name="hiddenUpdatedDate" value="<fmt:formatDate value="${user.updatedDate}" pattern="yyyy-MM-dd HH:mm:ss" />" />

						</td>
					</tr>
				</table>
				<input type="submit" value="編集" /> <br />
			</form>
		</div>
		<br/><br/><br/><br/><br/>
	    <div class="copyright">Copyright(c)Keisuke Izumo</div>
	</body>
</html>