<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>ホーム</title>
		<link href="./css/home.css" rel="stylesheet" type="text/css">
		<script>
			function disp(){
				if(window.confirm("削除しますか？")) {
					return true;
				} else {
					return false;
				}
			}
			function myfunc(ele) {
				var target = ele.id;
				var str = target.slice(11);
				var list = document.getElementById('list' + str);
				var status = window.getComputedStyle(list, null).getPropertyValue('display');
				if(status == 'none'){
					list.style.display = 'inline';
					document.getElementById( target ).value ="非表示";
				} else {
					list.style.display = 'none';
					document.getElementById( target ).value ="コメント表示";
				}
			}
			function searchClear(){
				var category = document.getElementById('searchCategory');
				var startDate = document.getElementById('startDate');
				var endDate = document.getElementById('endDate');
				category.value = "";
				startDate.value = "";
				endDate.value = "";
			}
		</script>
	</head>
	<body>
		<div class="menu">
			<ul>
				<li><a href="newMessage">新規投稿</a></li>
				<c:if test="${loginUser.departmentId == 1}">
					<li><a href="management">ユーザ管理</a></li>
				</c:if>
				<c:if test="${ not empty loginUser }">
					<li id="logout"><a href="logout">ログアウト</a></li>
				</c:if>
			</ul>
		</div>
		<br/>
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<div class="serch">
			<form action="./" method="get" >
			<div>
				<label for="searchCategory">カテゴリー：</label><input type="text" id="searchCategory" name="searchCategory" class="text" value="${searchCategory}"/>
			</div>
			<br/>
			<div>
				<label for="startDate">期間：</label><input type="date" id="startDate" name="startDate" class="text" value="${startDate}"/> ～
				<input type="date" id="endDate" name="endDate" class="text" value="${endDate}"/>
				<input type="submit" class="aaa" value="検索" />
				<input type="button" class="aaa" value="クリア" onclick="searchClear();"/>
			</div>
			</form>
		</div>
		<div class="message">
			<h3>投稿一覧</h3>
			<c:forEach items="${messageList}" var="message" >
				<div class="bbb">
					<c:if test="${message.userId == loginUser.id}">
						<div class="messageDelete">
							<form action="messageDelete" method="post" onsubmit="return disp();" >
								<p><input type="submit" class="aaa" value="投稿削除" /></p>
								<input type="hidden" name="hiddenMessageId" value="${message.id }" />
							</form>
						</div>
					</c:if>
					<div class="message-box">
						<div class="message-title"><c:out value="${message.title}" /></div>
						<div class="letter-body">
							<label for="category">カテゴリー:</label><span id="category"><c:out value="${message.category}" /></span>
							<pre class="wrap"><c:out value="${message.message}" /></pre>
						</div>
					</div>
					<div class="messageUser">
						<fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
						@<c:out value="${message.userName}" />
					</div>


					<div class="cp_menu">
						<input type="button" value="コメント表示" id="dispComment${message.id}" onclick="myfunc(this);" class="btn-partial-line" />
						<ul class="list" id="list${message.id}">
							<c:forEach items="${commentList}" var="commentList">
								<c:if test="${message.id == commentList.messageId }">
									<li>
										<div class="comment">
											<pre class="wrap"><c:out value="${commentList.text}" /></pre>
											<span class="user">
												<fmt:formatDate value="${commentList.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
												@<c:out value="${commentList.userName}" />
											</span>
										</div>
										<c:if test="${commentList.userId == loginUser.id}">
											<form action="commentDelete" method="post" onsubmit="return disp();" >
												<input type="submit" class="aaa" value="コメント削除" />
												<input type="hidden" name="hiddenCommentId" value="${commentList.id }"/>
											</form>
										</c:if>
									</li>
								</c:if>
							</c:forEach>
						</ul>
					</div>

					<div class="commentForm">
						<form action="newComment" method="post" >
							<textarea name="newComment" rows="5" cols="40"></textarea>
							<input type="hidden" name="hiddenMessageId" value="${message.id}" />
							<input type="hidden" name="commentFlag" value="true" />
							<input type="submit" class="aaa" value="コメント" />
						</form>
					</div>
				</div>
			</c:forEach>
		<div class="copyright"> Copyright(c)Keisuke Izumo</div>
		</div>
	</body>
</html>