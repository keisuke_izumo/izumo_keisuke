<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>ユーザ管理画面</title>
		<link href="./css/management.css" rel="stylesheet" type="text/css">
		<script>
			function disp(){
				if(window.confirm("ユーザの停止状態を変更しますか？")) {
					return true;
				} else {
					return false;
				}

			}
		</script>
	</head>
	<body>
		<div class="menu">
			<ul>
				<li><a href="./">HOME</a></li>
				<li><a href="signup">ユーザ登録</a></li>
				<c:if test="${ not empty loginUser }">
					<li id="logout"><a href="logout">ログアウト</a></li>
				</c:if>
			</ul>
		</div>
		<c:if test="${ not empty errorMessages }">
            <div class="errorMessages">
                <ul>
                    <c:forEach items="${errorMessages}" var="message">
                        <li><c:out value="${message}" />
                    </c:forEach>
                </ul>
            </div>
            <c:remove var="errorMessages" scope="session"/>
        </c:if>
		<div class="userList">
			<h3>ユーザ一覧</h3>
			<table border="1">
				<tr>
					<th>編集</th>
					<th>ログインID</th>
					<th>名前</th>
					<th>支店名</th>
					<th>部署・役職名</th>
					<th id="stoped">停止状態</th>
					<th id="createdDate">登録日</th>
					<th id="updatedDate">更新日</th>
				</tr>
			    <c:forEach items="${userList}" var="userList">
			            <tr>
			            	<td><a class="link" href="<c:url value="edit">
			            			<c:param name="parameter" value="${userList.id}" />
			            			</c:url>">編
			            		</a>
			            	</td>
			                <td><c:out value="${userList.loginId}" /></td>
			                <td><c:out value="${userList.name}" /></td>
			                <td><c:out value="${userList.branchName}" /></td>
			                <td><c:out value="${userList.departmentName}" /></td>
			                <td>
			                <c:if test="${userList.id != loginUser.id }" >
								<c:choose>
			  						<c:when test="${userList.isStoped == '1'}"><c:set var="isStoped">復活する</c:set></c:when>
			  						<c:otherwise><c:set var="isStoped">停止する</c:set></c:otherwise>
								</c:choose>
								<form action="stoped" method="post" onsubmit="return disp();">
									<input type="submit" name="isStoped" value=<c:out value="${isStoped}" /> />
									<input type="hidden" name="hiddenIsStoped" value="${userList.isStoped }" />
									<input type="hidden" name="hiddenId" value="${userList.id }" />
								</form>
							</c:if>
							<c:if test="${userList.id == loginUser.id }" >
								<p>ログイン中</p>
							</c:if>
							</td>
			                <td><c:out value="${userList.createdDate}" /></td>
			                <td><c:out value="${userList.updatedDate}" /></td>
			            </tr>
			    </c:forEach>
			 </table>
		<div class="copyright"> Copyright(c)Keisuke Izumo</div>
		</div>
	</body>
</html>