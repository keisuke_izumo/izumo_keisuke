<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>新規投稿画面</title>
		<link href="./css/newMessage.css" rel="stylesheet" type="text/css">
		<script>
			function disp(){
				if(window.confirm("この内容で登録しますか？")) {
					return true;
				} else {
					return false;
				}
			}
		</script>
	</head>
	<body>
		<div class="menu">
			<ul>
				<li><a href="./">HOME</a></li>
				<c:if test="${ not empty loginUser }">
					<li id="logout"><a href="logout">ログアウト</a></li>
				</c:if>
			</ul>
		</div>
		<div class="main-contents">
			<c:if test="${ not empty errorMessages }">
	            <div class="errorMessages">
	                <ul>
	                    <c:forEach items="${errorMessages}" var="message">
	                        <li><c:out value="${message}" />
	                    </c:forEach>
	                </ul>
	            </div>
	            <c:remove var="errorMessages" scope="session"/>
	        </c:if>
			<div class="form-area">
				<form action="newMessage" method="post" onsubmit="return disp();">
					<!-- <label for="category">カテゴリー：</label><br/><br/>
					<label for="title">件名：</label><br/><br/>
					<label for="message">本文：</label><br/><br/>
					 -->
					<table class="messageTable">
						<tr>
							<th>カテゴリー</th>
							<td>
								<input type="text" name="category" id="category" value="${message.category}"/>
							</td>
						</tr>
						<tr>
							<th>件名</th>
							<td>
								<input type="text" name="title" id="title" value="${message.title }"/>
							</td>
						</tr>
						<tr>
							<th>本文</th>
							<td>
								<textarea name="message" id="message" rows="5" cols="40" ><c:out value="${message.message}"></c:out></textarea>
							</td>
						</tr>
					</table>
					<input type="submit" value="投稿する">
				</form>
			</div>
		</div>
		<br/><br/><br/><br/><br/>
	    <footer class="copyright"><p>Copyright(c)Keisuke Izumo</p></footer>
	</body>
</html>