<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー登録</title>
    <link href="./css/signup.css" rel="stylesheet" type="text/css">
    <script>
		function disp(){
			if(window.confirm("この内容で登録しますか？")) {
				return true;
			} else {
				return false;
			}
		}
	</script>
    </head>
    <body>
    	<div class="menu">
			<ul>
				<li><a href="./">HOME</a></li>
				<li><a href="management">ユーザ管理</a></li>
				<c:if test="${ not empty loginUser }">
					<li id="logout"><a href="logout">ログアウト</a></li>
				</c:if>
			</ul>
		</div>
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
	        <form action="signup" method="post" onsubmit="return disp();">
				<table class="signupTable">
					<tr>
						<th>ログインID</th>
						<td>
							<input type="text" name="loginId" value="${user.loginId}">
						</td>
					</tr>
					<tr>
						<th>名前</th>
						<td>
							<input type="text" name="name" value="${user.name}">
						</td>
					</tr>
					<tr>
						<th>パスワード</th>
						<td>
							<input type="password" name="password">
						</td>
					</tr>
					<tr>
						<th>パスワード確認用</th>
						<td>
							<input type="password" name="confirmPass">
						</td>
					</tr>
					<tr>
						<th>支店</th>
						<td>
						<!-- リストで支店名と役職名 -->
							<select name="branchList">
								<c:forEach items="${branch}" var="branch">
									<c:choose>
										<c:when test="${user.branchId == branch.branchId }">
											<option value="${branch.branchId }" selected><c:out value="${branch.branchName}" /></option>
										</c:when>
										<c:otherwise>
											<option value="${branch.branchId }"><c:out value="${branch.branchName}" /></option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr>
						<th>部署・役職</th>
						<td>
							<select name="departmentList">
								<c:forEach items="${department}" var="department">
									<c:choose>
										<c:when test="${user.departmentId == department.departmentId }">
											<option value="${department.departmentId }" selected><c:out value="${department.departmentName}" /></option>
										</c:when>
										<c:otherwise>
											<option value="${department.departmentId }"><c:out value="${department.departmentName}" /></option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</td>
					</tr>
				</table>
				<input type="submit" value="登録" />
			</form>
		</div>
		<br/><br/><br/><br/><br/>
	    <div class="copyright">Copyright(c)Keisuke Izumo</div>
    </body>
</html>